
Login buyer:
	url : /login/buyer
	method : post
	params :
		- email (string)
		- password (string)
	response :
		- status : 200 / 404


signin buyer:
	url : /signin/buyer
	method : post
	params:
		- lastName (string)
		- firstName (string)
		- email (string)
		- emailCodeCheck (string)
		- password (string)
		- passwordConfirmation (string)
		- phone (string)
		- phoneCodeCheck (string)
		- iban (string)
		- deliveryAddress (string)
		- deliveryCity (string)
		- deliveryPostalCode (string)
		- country (string)
		- createdAt (datetime)
		- updatedAt (datetime)
	response :
		- status : 200 / 500


buyer phone confirm request: [generate and send sms code]
	url : /buyer/phone/confirm/request
	method : post
	params:
		- buyerId (numeric)	
	response :
		- status : 200 / 404 / 500

buyer phone validation:
	url : /buyer/phone/validation
	method : post
	params:
		- buyerId (numeric)
		- code (string)	
	response :
		- status : 200 / 404 /500

buyer email confirm request: [generate and send email code]
	url : /buyer/email/confirm/request
	method : post
	params:
		- buyerId (numeric)	
	response :
		- status : 200 / 404 / 500

buyer email validation:
	url : /buyer/email/validation
	method : post
	params:
		- buyerId (numeric)
		- code (string)	
	response :
		- status : 200 / 404 /500


profil buyer udpdate:
	url : /buyer/update
	method : post
	params:
		- buyerId
		- lastName (string)
		- firstName (string)
		- email (string)
		- emailCodeCheck (string)
		- password (string)
		- passwordConfirmation (string)
		- phone (string)
		- phoneCodeCheck (string)
		- iban (string)
		- deliveryAddress (string)
		- deliveryCity (string)
		- deliveryPostalCode (string)
		- country (string)

	response :
		- status : 200 / 404 / 500


-------------------------

Login farm:
	url : /login/farm
	method : post
	params :
		- email (string)
		- password (string)
	response :
		- status : 200 / 404

signin farm:
	url : /signin/farm
	method : post
	params:
		- name (string)
		- address (string)
		- typeOperation (numeric)
		- country (string)
		- homeDelivery
		- memberShipType (relation ManyToOne memberShipType)
		- images (files)
		- createdAt (datetime)
		- updatedAt (datetime)
	response :
		- status : 200 / 500

#memberShipType

Profil farm update:
	url : /farm/update
	method : post
	params:
		- farmId (numeric)
		- name (string)
		- address (string)
		- typeOperation (numeric)
		- memberShipType
		- images[] (array)
	response :
		- status : 200 / 404/ 500


-------------------------
#productCategory

product create:
	url: /farm/product/create
	method : post
	params:
		- farmId (numeric)
		- name (string)
		- unity (string)
		- price (decimal)
		- stock (float)
		- description (text)
		- reduction
		- images (files)
		- recipe (files)
		- categoryProduct (relation ManyToMany categoryProduct)
		- createdAt (datetime)
		- updatedAt (datetime)
	response :
		- status : 200 / 500

product update (fait)
	url: /farm/product/update
	method : post
	params:
		- farmId (numeric)
		- productId (numeric)
		- name (string)
		- unity (string)
		- price (decimal)
		- stock (float)
		- description (text)
		- reduction
		- images (files)
		- recipe (files)
		- categoryProduct (relation ManyToMany categoryProduct)
	response :
		- status : 200 / 404 / 500


products list:
	url: /farm/products
	method : get
	params:
		- farmId (numeric)
	response :
		- results :
			- products
		- status : 200 / 404/ 500

products farm search:
	url: /farm/product/search
	method : get
	params:
		- farmId (numeric)
		- filters (string)
	response :
		- results :
			- products
		- status : 200 / 404/ 500

products search:
	url: /product/search
	method : get
	params:
		- filters (string)
	response :
		- results :
			- products
		- status : 200 / 404/ 500

